/**
 * @description Class to manage Waiter activities
 * @author Javaid
 */
import { Table } from './Table';
import { WAITER } from './statuses';
export class Waiter {
    private static waiterNumber = 0;
    private waiterNumber;
    private busy = false;
    private tables = [];
    private status = WAITER.FREE;
    private preFix = "waiter";

    constructor() {
        // Increment the waiter number on each creation.
        this.waiterNumber = `${this.preFix} ${++Waiter.waiterNumber}`;
    }

    /**
     * To assign tables to waiter and 
     * In assign the table, It will get reserved automatically
     * @param {Table[]} tables
     * @returns Table[] 
     */
    assignTable(tables: Table[]) {
        // Can not assign more tables to busy waiter.
        if (this.busy) {
            return false;
        }

        // If waiter is free than tables will get assign to waiter.
        this.busy = true;
        let tableNumbers = [];
        tables.forEach((singleTable: Table) => {
            singleTable.markReserved();
            tableNumbers.push(singleTable.getTableNumber());
            this.tables.push(singleTable);
        });
        this.status = `${WAITER.SERVING} ${tableNumbers.join(",")}`
        return this.tables;
    }

    /**
     * To get number or name of waiter
     * @returns {String} name/number of waiter  
     */
    getWaiterNumber() {
        return this.waiterNumber;
    }

    /**
     * To get list of assigned tables
     * @returns {Table[]}
     */
    getTables() {
        return this.tables;
    }

    /**
     * To check waiter state
     * @returns {Boolean}
     */
    isBusy() {
        return this.busy;
    }

    /**
     * To get current status of waiter
     * @returns {String}
     */
    getStatus() {
        return this.status;
    }

    /**
     * Waiter will clean the table if it is free and reserve table for 1 sec while table is getting clean.
     * @param {Table} table
     * @returns {Table[]} list of current assigned table 
     */
    cleanTable(table: Table) {
        if (this.busy) {
            return false;
        }
        this.busy = true;
        table.markReserved();
        this.status = `${WAITER.CLEANING} ${table.getTableNumber()}`;
        setTimeout(() => {
            this.free();
        }, 1000);

        return this.tables.push(table);
    }

    /**
     * To make free waiter from current assigned tables.
     * @returns void
     */
    free() {
        this.tables.forEach((singleTable: Table) => {
            singleTable.free();
        })
        this.tables = [];
        this.status = WAITER.FREE;
        this.busy = false;
    }
}