export const WAITER = {
    FREE: "Free",
    SERVING: "Serving table(s)",
    CLEANING: "Cleaning table"
}