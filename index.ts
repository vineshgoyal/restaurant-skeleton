import { Restaurant } from './Restaurant';

const restaurant = new Restaurant();

/**
 * Restaurant will reserve two tables because only 4 people can use a single table.
 * and reserved tables will assign to a free waiter
 */
try {
    restaurant.reserveTable(5);
    console.log(restaurant);
} catch (e) {
    console.error("Error", e.message);
}

/**
 * Restaurant will reserve two tables for 8 people because only 4 people can use a single table.
 * and reserved tables will assign to a free waiter
 */
try {
    restaurant.reserveTable(8);
    console.log(restaurant);
} catch (e) {
    console.error("Error", e.message);
}


/**
 * Need 4 tables for 13 people and 4 tables are being used by other customers.
 * It will throw error because now restaurant have only 3 free tables.
 */
try {
    restaurant.reserveTable(13);
    console.log(restaurant);
} catch (e) {
    console.error("Error", e.message);
}

/**
 * Waiter can clean the table because table number 6 is not being used.
 */
try {
    let tableClean = restaurant.cleanTable(6);
    if (tableClean) {
        console.warn("Table is getting cleaned")
    }
} catch (e) {
    console.error("Error", e.message);
}

/**
 * Waiter can not clean the table because table number 1 is being used.
 */
try {
    restaurant.cleanTable(1);
} catch (e) {
    console.error("Error", e.message);
}


