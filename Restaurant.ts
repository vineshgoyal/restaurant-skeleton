/**
 * @description A Restaurant to manage all waiter and table for customer.
 * @author Javaid
 */

import { Table } from './Table';
import { Waiter } from './Waiter';
export class Restaurant {
    private tables = [];
    private waiters = [];
    private maxCapacityOfTable = 4;
    constructor() {
        /**
         * Setup restaurant with its assets
         */

        // Adding 7 tables in restaurant 
        for (let t = 0; t < 7; t++) {
            this.tables.push(new Table());
        }

        // Adding 4 waiters
        for (let w = 0; w < 7; w++) {
            this.waiters.push(new Waiter());
        }
    }

    private getFreeTables(requiredTables: number) {
        let numberOfTables = [];
        for (let i = 0; i < this.tables.length; i++) {
            let table: Table = this.tables[i];
            if (!table.isReserved()) {
                numberOfTables.push(table);
            }
            if (numberOfTables.length >= requiredTables) {
                break;
            }
        }

        return numberOfTables.length === requiredTables ? numberOfTables : null;
    }

    private getTableByNumber(numberOfTable: number) {
        return this.tables[numberOfTable - 1] || null;
    }

    private getFreeWaiter() {
        let freeWaiter = null;
        this.waiters.every((waiter: Waiter) => {
            freeWaiter = waiter;
            return waiter.isBusy()
        });
        return freeWaiter;
    }

    reserveTable(numberOfPeople: number) {
        const requiredTables = Math.ceil(numberOfPeople / this.maxCapacityOfTable);
        const waiter: Waiter = this.getFreeWaiter();
        if (!waiter) {
            throw new Error(`All waiters are busy right now, please wait for sometime.`);
        }
        let freeTables = this.getFreeTables(requiredTables);
        if (freeTables) {
            waiter.assignTable(freeTables);
            return true;
        } else {
            throw new Error(`Required ${requiredTables} Table(s) are not available.`)
        }
    }

    cleanTable(tableNumber: number) {
        const waiter: Waiter = this.getFreeWaiter();
        const table: Table = this.getTableByNumber(tableNumber);

        if (!table) {
            throw new Error(`Invalid table number #${tableNumber}.`);
        }
        if (table.isReserved()) {
            throw new Error(`The table #${tableNumber} cannot be cleaned since its being used by customer.`);
        }
        waiter.cleanTable(table);
        return true;
    }
}