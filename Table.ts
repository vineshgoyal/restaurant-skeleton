/**
 * @description Class to manage Tables
 * @author Javaid
 */

export class Table {
    private static tableNumber = 0;
    private tableNumber = 0;
    private maxPeople = 4;
    private reserved = false;
    constructor() {
        this.tableNumber = ++Table.tableNumber;
    }

    getTableNumber() {
        return this.tableNumber;
    }

    getCapacity() {
        return this.maxPeople;
    }

    markReserved() {
        return this.reserved = true;
    }

    isReserved() {
        return this.reserved;
    }

    free() {
        return this.reserved = false;
    }
}
